package com.smartstep.oceanapp.Models;

import com.google.gson.annotations.SerializedName;

public class BaseResponse {
    @SerializedName("statusCode") private int statusCode = 0;
    @SerializedName("message") private String message = "";
    @SerializedName("data") private Object data = null;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
