package com.smartstep.oceanapp.Models.Start_models;

import com.google.gson.annotations.SerializedName;

public class LogoObject {
    @SerializedName("logo") private String logo = "";


    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
