package com.smartstep.oceanapp.Models.Start_models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class StartResponse {
    @SerializedName("config") private List<StartObject> config = new ArrayList<>();
    @SerializedName("system") private LogoObject system = new LogoObject();

    public List<StartObject> getConfig() {
        return config;
    }

    public void setConfig(List<StartObject> config) {
        this.config = config;
    }

    public LogoObject getSystem() {
        return system;
    }

    public void setSystem(LogoObject system) {
        this.system = system;
    }
}
