package com.smartstep.oceanapp.Models.Start_models;

import com.google.gson.annotations.SerializedName;

public class LocationObject {
    @SerializedName("lat") private String lat = "";
    @SerializedName("lng") private String lng = "";

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
