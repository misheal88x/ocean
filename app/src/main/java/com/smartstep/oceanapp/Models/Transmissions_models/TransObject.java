package com.smartstep.oceanapp.Models.Transmissions_models;

import com.google.gson.annotations.SerializedName;

public class TransObject {
    @SerializedName("unit1_id") private String unit1_id = "";
    @SerializedName("unit1_name") private String unit1_name = "";
    @SerializedName("unit1_shortcut") private String unit1_shortcut = "";
    @SerializedName("unit1_flag_icon_url") private String unit1_flag_icon_url = "";
    @SerializedName("unit2_id") private String unit2_id = "";
    @SerializedName("unit2_name") private String unit2_name = "";
    @SerializedName("unit2_shortcut") private String unit2_shortcut = "";
    @SerializedName("unit2_flag_icon_url") private String unit2_flag_icon_url = "";
    @SerializedName("original_amount") private String original_amount = "";
    @SerializedName("amount_after_convert") private String amount_after_convert = "";
    @SerializedName("visibility_flag") private String visibility_flag = "";
    @SerializedName("last_changes_ratio") private String last_changes_ratio = "";

    public String getUnit1_id() {
        return unit1_id;
    }

    public void setUnit1_id(String unit1_id) {
        this.unit1_id = unit1_id;
    }

    public String getUnit1_name() {
        return unit1_name;
    }

    public void setUnit1_name(String unit1_name) {
        this.unit1_name = unit1_name;
    }

    public String getUnit1_shortcut() {
        return unit1_shortcut;
    }

    public void setUnit1_shortcut(String unit1_shortcut) {
        this.unit1_shortcut = unit1_shortcut;
    }

    public String getUnit1_flag_icon_url() {
        return unit1_flag_icon_url;
    }

    public void setUnit1_flag_icon_url(String unit1_flag_icon_url) {
        this.unit1_flag_icon_url = unit1_flag_icon_url;
    }

    public String getUnit2_id() {
        return unit2_id;
    }

    public void setUnit2_id(String unit2_id) {
        this.unit2_id = unit2_id;
    }

    public String getUnit2_name() {
        return unit2_name;
    }

    public void setUnit2_name(String unit2_name) {
        this.unit2_name = unit2_name;
    }

    public String getUnit2_shortcut() {
        return unit2_shortcut;
    }

    public void setUnit2_shortcut(String unit2_shortcut) {
        this.unit2_shortcut = unit2_shortcut;
    }

    public String getUnit2_flag_icon_url() {
        return unit2_flag_icon_url;
    }

    public void setUnit2_flag_icon_url(String unit2_flag_icon_url) {
        this.unit2_flag_icon_url = unit2_flag_icon_url;
    }

    public String getOriginal_amount() {
        return original_amount;
    }

    public void setOriginal_amount(String original_amount) {
        this.original_amount = original_amount;
    }

    public String getAmount_after_convert() {
        return amount_after_convert;
    }

    public void setAmount_after_convert(String amount_after_convert) {
        this.amount_after_convert = amount_after_convert;
    }

    public String getVisibility_flag() {
        return visibility_flag;
    }

    public void setVisibility_flag(String visibility_flag) {
        this.visibility_flag = visibility_flag;
    }

    public String getLast_changes_ratio() {
        return last_changes_ratio;
    }

    public void setLast_changes_ratio(String last_changes_ratio) {
        this.last_changes_ratio = last_changes_ratio;
    }
}
