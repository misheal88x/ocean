package com.smartstep.oceanapp.Models.Transmissions_models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class TransResponse {
    @SerializedName("units") private List<TransObject> units = new ArrayList<>();
    @SerializedName("last_update_date") private String last_update_date = "";

    public List<TransObject> getUnits() {
        return units;
    }

    public void setUnits(List<TransObject> units) {
        this.units = units;
    }

    public String getLast_update_date() {
        return last_update_date;
    }

    public void setLast_update_date(String last_update_date) {
        this.last_update_date = last_update_date;
    }
}
