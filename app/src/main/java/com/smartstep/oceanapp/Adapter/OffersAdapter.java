package com.smartstep.oceanapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smartstep.oceanapp.Models.Offers_models.OfferObject;
import com.smartstep.oceanapp.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.ViewHolder> {
    private Context context;
    private List<OfferObject> list;
    private String[] titles = new String[]{"عرض عيد الفصح","عرض عيد الأم"};
    private String[] sub_titles = new String[]{"تخفيض بقيمة 40% على كل عمليات التحويل","تخفيض بقيمة 50% على كل عمليات التحويل"};


    public OffersAdapter(Context context,List<OfferObject> list) {
        this.context = context;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView title,sub_title;
        private CardView layout;
        private RelativeLayout whatsapp;
        public ViewHolder(View view) {
            super(view);

            title = view.findViewById(R.id.title);
            sub_title = view.findViewById(R.id.sub_title);
            whatsapp = view.findViewById(R.id.whatsapp);
            layout = view.findViewById(R.id.layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_offer, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        OfferObject o = list.get(position);
        holder.title.setText(o.getTitle());
        holder.sub_title.setText(o.getFull_description());
    }
}
