package com.smartstep.oceanapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smartstep.oceanapp.Bases.BaseFunctions;
import com.smartstep.oceanapp.Bases.SharedPrefManager;
import com.smartstep.oceanapp.Models.Services_models.ServiceObject;
import com.smartstep.oceanapp.Models.Start_models.StartObject;
import com.smartstep.oceanapp.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class ServicesAdapter  extends RecyclerView.Adapter<ServicesAdapter.ViewHolder> {
    private Context context;
    private List<ServiceObject> list;
    private String title = "خدمات التحويل";
    private String sub_title = "هذا لنص هو مثال لنص يمكن أن يستبدل في نفس المساحة";


    public ServicesAdapter(Context context,List<ServiceObject> list) {
        this.context = context;
        this.list = list;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView title,sub_title;
        private CardView layout;
        private RelativeLayout whatsapp;
        public ViewHolder(View view) {
            super(view);

            title = view.findViewById(R.id.title);
            sub_title = view.findViewById(R.id.sub_title);
            whatsapp = view.findViewById(R.id.whatsapp);
            layout = view.findViewById(R.id.layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_service, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        ServiceObject o = list.get(position);

        holder.title.setText(o.getTitle());
        holder.sub_title.setText(o.getFull_description());
        holder.whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number = "";
                for (StartObject s : SharedPrefManager.getInstance(context).getStartResponse()){
                    if (s.getSlug_name().equals("contact_us.whatsapp_phone")){
                        number = s.getValue().toString();
                        break;
                    }
                }
                if (number!= null &&!number.equals("")){
                    BaseFunctions.openWhatsapp(context,number.substring(1));
                }else {
                    BaseFunctions.showErrorToast(context,"لا يوجد رقم واتساب");
                }
            }
        });
    }
}
