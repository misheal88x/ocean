package com.smartstep.oceanapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.smartstep.oceanapp.Activities.MainActivity;
import com.smartstep.oceanapp.Bases.BaseFunctions;
import com.smartstep.oceanapp.Fragments.CalculatorFragment;
import com.smartstep.oceanapp.Models.Transmissions_models.TransObject;
import com.smartstep.oceanapp.R;

import java.io.File;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class TransmissionsAdapter extends RecyclerView.Adapter<TransmissionsAdapter.ViewHolder> {
    private Context context;
    private List<TransObject> list;
    private String type;


    public TransmissionsAdapter(Context context,List<TransObject> list,String type) {
        this.context = context;
        this.list = list;
        this.type = type;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView from_flag;
        private ImageView left_arrow,average;
        private TextView from_currency,to_currency,value;
        private CardView layout;
        private LinearLayout back_layout,go_to_cal;
        public ViewHolder(View view) {
            super(view);
            from_flag = view.findViewById(R.id.from_flag);
            from_currency = view.findViewById(R.id.from_currency);
            left_arrow = view.findViewById(R.id.left_arrow);
            average = view.findViewById(R.id.average);
            to_currency = view.findViewById(R.id.to_currency);
            value = view.findViewById(R.id.value);
            layout = view.findViewById(R.id.layout);
            back_layout = view.findViewById(R.id.back_layout);
            go_to_cal = view.findViewById(R.id.go_to_cal);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_transmission, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        final TransObject o = list.get(position);
        holder.from_currency.setText(o.getUnit1_name());
        BaseFunctions.setFrescoImage(holder.from_flag,o.getUnit1_flag_icon_url());
        //BaseFunctions.setGlideImage(context,holder.from_flag,o.getUnit1_flag_icon_url());
        holder.to_currency.setText(o.getUnit2_name());
        //BaseFunctions.setGlideImage(context,holder.to_flag,o.getUnit2_flag_icon_url());
        if (type.equals("before")){
            holder.value.setText(o.getOriginal_amount());
        }else {
            holder.value.setText(o.getAmount_after_convert());
        }

        if (o.getLast_changes_ratio()!=null && !o.getLast_changes_ratio().equals("") &&
        !o.getLast_changes_ratio().equals("0")&&!o.getLast_changes_ratio().equals("0.00")){
            float value = Float.valueOf(o.getLast_changes_ratio());
            if (value>0){
                holder.average.setImageResource(R.drawable.up_green);
            }else if (value<0){
                holder.average.setImageResource(R.drawable.down_red);
            }
        }else {
            holder.average.setVisibility(View.GONE);
        }
        holder.layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                if (arg1.getAction()==MotionEvent.ACTION_DOWN) {
                    holder.back_layout.setBackgroundResource(R.drawable.gradient_back);
                    holder.from_currency.setTextColor(context.getResources().getColor(R.color.white));
                    holder.to_currency.setTextColor(context.getResources().getColor(R.color.white));
                    holder.value.setTextColor(context.getResources().getColor(R.color.white));
                    holder.left_arrow.setImageResource(R.drawable.left_arrow_white);
                    if (o.getLast_changes_ratio()!=null && !o.getLast_changes_ratio().equals("") &&
                            !o.getLast_changes_ratio().equals("0")&&!o.getLast_changes_ratio().equals("0.00")){
                        float value = Float.valueOf(o.getLast_changes_ratio());
                        if (value>0){
                            holder.average.setImageResource(R.drawable.up_white);
                        }else if (value<0){
                            holder.average.setImageResource(R.drawable.down_white);
                        }
                    }
                }
                else if (arg1.getAction()==MotionEvent.ACTION_UP) {
                    holder.back_layout.setBackgroundColor(context.getResources().getColor(R.color.white));
                    holder.from_currency.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    holder.to_currency.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    holder.value.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    holder.left_arrow.setImageResource(R.drawable.left_arrow);
                    if (o.getLast_changes_ratio()!=null && !o.getLast_changes_ratio().equals("") &&
                            !o.getLast_changes_ratio().equals("0")&&!o.getLast_changes_ratio().equals("0.00")){
                        float value = Float.valueOf(o.getLast_changes_ratio());
                        if (value>0){
                            holder.average.setImageResource(R.drawable.up_green);
                        }else if (value<0){
                            holder.average.setImageResource(R.drawable.down_red);
                        }
                    }
                }
                return true;
            }
        });
        holder.go_to_cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("from_unit",o.getUnit1_name());
                bundle.putString("to_unit",o.getUnit2_name());
                bundle.putString("to_unit_symbol",o.getUnit2_shortcut());
                if (type.equals("before")){
                    bundle.putString("value",o.getOriginal_amount());
                }else {
                    bundle.putString("value",o.getAmount_after_convert());
                }
                CalculatorFragment fragment = new CalculatorFragment();
                fragment.setArguments(bundle);
                ((MainActivity)context).open_fragment(fragment);
            }
        });
    }
}
