package com.smartstep.oceanapp.APIsClasses;

import android.content.Context;

import com.smartstep.oceanapp.APIs.ServicesAPIs;
import com.smartstep.oceanapp.APIs.UserAPIs;
import com.smartstep.oceanapp.Bases.BaseFunctions;
import com.smartstep.oceanapp.Bases.BaseRetrofit;
import com.smartstep.oceanapp.Interfaces.IFailure;
import com.smartstep.oceanapp.Interfaces.IResponse;
import com.smartstep.oceanapp.Models.BaseResponse;
import com.smartstep.oceanapp.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class UserAPIsClass extends BaseRetrofit {
    public static void update_token(
            final Context context,
            String token,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithoutBearer();
        UserAPIs api = retrofit.create(UserAPIs.class);
        Call<Object> call = api.update_token(BaseFunctions.getDeviceId(context),token);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (response.isSuccessful()){
                    if (response.code() == 200){
                        if (response.body()!=null){
                            onResponse.onResponse(response.body());
                        }else {
                            onResponse.onResponse();
                        }
                    }else {
                        onResponse.onResponse();
                    }
                }else {
                    try {
                        BaseFunctions.showErrorToast(context, context.getResources().getString(R.string.error_occured_2));
                    }catch (Exception e){
                        BaseFunctions.showErrorToast(context, context.getResources().getString(R.string.error_occured_2));
                    }
                    onResponse.onResponse();
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
}
