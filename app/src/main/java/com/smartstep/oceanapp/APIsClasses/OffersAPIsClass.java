package com.smartstep.oceanapp.APIsClasses;

import android.content.Context;

import com.smartstep.oceanapp.APIs.OffersAPIs;
import com.smartstep.oceanapp.APIs.ServicesAPIs;
import com.smartstep.oceanapp.Bases.BaseFunctions;
import com.smartstep.oceanapp.Bases.BaseRetrofit;
import com.smartstep.oceanapp.Bases.SharedPrefManager;
import com.smartstep.oceanapp.Interfaces.IFailure;
import com.smartstep.oceanapp.Interfaces.IResponse;
import com.smartstep.oceanapp.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class OffersAPIsClass extends BaseRetrofit {
    public static void get_offers(
            final Context context,
            int page,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithoutBearer();
        OffersAPIs api = retrofit.create(OffersAPIs.class);
        Call<BaseResponse> call = api.get_offers(
                BaseFunctions.getDeviceId(context),
                page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
}
