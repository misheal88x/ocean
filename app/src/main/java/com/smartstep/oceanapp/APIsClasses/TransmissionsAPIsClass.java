package com.smartstep.oceanapp.APIsClasses;

import android.content.Context;

import com.smartstep.oceanapp.APIs.ServicesAPIs;
import com.smartstep.oceanapp.APIs.TransmissionsAPIs;
import com.smartstep.oceanapp.Bases.BaseFunctions;
import com.smartstep.oceanapp.Bases.BaseRetrofit;
import com.smartstep.oceanapp.Interfaces.IFailure;
import com.smartstep.oceanapp.Interfaces.IResponse;
import com.smartstep.oceanapp.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class TransmissionsAPIsClass extends BaseRetrofit {

    public static void get_units(
            final Context context,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithoutBearer();
        TransmissionsAPIs api = retrofit.create(TransmissionsAPIs.class);
        Call<BaseResponse> call = api.get_units(BaseFunctions.getDeviceId(context));
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void get_unit(
            final Context context,
            String id,
            IResponse onResponse1,
            IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithoutBearer();
        TransmissionsAPIs api = retrofit.create(TransmissionsAPIs.class);
        Call<BaseResponse> call = api.getUnit(BaseFunctions.getDeviceId(context),id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(context,response,onResponse);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
}
