package com.smartstep.oceanapp.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.smartstep.oceanapp.Activities.MainActivity;
import com.smartstep.oceanapp.Bases.App;
import com.smartstep.oceanapp.Bases.BaseFragment;
import com.smartstep.oceanapp.Bases.BaseFunctions;
import com.smartstep.oceanapp.Bases.SharedPrefManager;
import com.smartstep.oceanapp.Models.Start_models.LocationObject;
import com.smartstep.oceanapp.Models.Start_models.StartObject;
import com.smartstep.oceanapp.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class ContactFragment extends BaseFragment implements OnMapReadyCallback {

    private GoogleMap map;
    private RelativeLayout whatsapp;
    private TextView email,phone;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contact,container,false);
    }
    @Override
    public void init_views() {
         whatsapp = base.findViewById(R.id.whatsapp);
         email = base.findViewById(R.id.email);
         phone = base.findViewById(R.id.phone);
    }

    @Override
    public void init_events() {
        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number = "";
                for (StartObject s : SharedPrefManager.getInstance(base).getStartResponse()){
                    if (s.getSlug_name().equals("contact_us.whatsapp_phone")){
                        number = s.getValue().toString();
                        break;
                    }
                }
                if (number!= null &&!number.equals("")){
                    BaseFunctions.openWhatsapp(base,number.substring(1));
                }else {
                    BaseFunctions.showErrorToast(base,"لا يوجد رقم واتساب");
                }
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        if (App.contact!=null && !App.contact.equals("")){
            ((TextView)base.findViewById(R.id.title)).setText(App.contact);
        }
        if (App.about!=null && !App.about.equals("")){
            ((TextView)base.findViewById(R.id.about_us)).setText(App.about);
        }
        ((MainActivity)base).refresh_btn.setVisibility(View.GONE);
        ((MainActivity)base).search_layout.setVisibility(View.GONE);
        initMap();
        for (StartObject s : SharedPrefManager.getInstance(base).getStartResponse()){
            if (s.getSlug_name().equals("contact_us.whatsapp_phone")){
                phone.setText("موبايل: "+s.getValue().toString());
            }else if (s.getSlug_name().equals("contact_us.email")){
                email.setText("إيميل: "+s.getValue().toString());
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        double lat = 0.0;
        double lng = 0.0;
        for (StartObject s : SharedPrefManager.getInstance(base).getStartResponse()){
            if (s.getSlug_name().equals("app_config.gps_location")){
                String j = new Gson().toJson(s.getValue());
                LocationObject lo = new Gson().fromJson(j,LocationObject.class);
                lat = Double.valueOf(lo.getLat());
                lng = Double.valueOf(lo.getLng());
            }
        }
        map.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)).position(new LatLng(lat,lng)).title("My Location"));
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat,lng),15.0f));
    }

    private void initMap(){
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(ContactFragment.this);
    }

}
