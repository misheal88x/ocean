package com.smartstep.oceanapp.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.smartstep.oceanapp.APIsClasses.TransmissionsAPIsClass;
import com.smartstep.oceanapp.Activities.MainActivity;
import com.smartstep.oceanapp.Adapter.TransmissionsAdapter;
import com.smartstep.oceanapp.Bases.BaseFragment;
import com.smartstep.oceanapp.Interfaces.IFailure;
import com.smartstep.oceanapp.Interfaces.IResponse;
import com.smartstep.oceanapp.Models.Transmissions_models.TransObject;
import com.smartstep.oceanapp.Models.Transmissions_models.TransResponse;
import com.smartstep.oceanapp.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SearchResultFragment extends BaseFragment {
    private RecyclerView recyclerView;
    private TransmissionsAdapter adapter;
    private LinearLayoutManager layoutManager;
    private List<TransObject> list;

    private LinearLayout loading_layout,no_data_layout,error_layout,no_internet_layout;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search_result,container,false);
    }

    @Override
    public void init_views() {
        recyclerView = base.findViewById(R.id.recycler);
        loading_layout = base.findViewById(R.id.loading_layout);
        no_data_layout = base.findViewById(R.id.no_data_layout);
        error_layout = base.findViewById(R.id.error_layout);
        no_internet_layout = base.findViewById(R.id.no_internet_layout);
    }

    @Override
    public void init_events() {
        error_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callAPI();
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        ((MainActivity)base).refresh_btn.setVisibility(View.GONE);
        ((MainActivity)base).search_layout.setVisibility(View.GONE);
        init_recycler();
        callAPI();
    }

    private void init_recycler(){
        list = new ArrayList<>();
        adapter = new TransmissionsAdapter(base,list,getArguments().getString("type"));
        layoutManager = new LinearLayoutManager(base);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void callAPI(){
        loading_layout.setVisibility(View.VISIBLE);
        no_data_layout.setVisibility(View.GONE);
        no_internet_layout.setVisibility(View.GONE);
        error_layout.setVisibility(View.GONE);
        TransmissionsAPIsClass.get_units(base, new IResponse() {
            @Override
            public void onResponse() {
                loading_layout.setVisibility(View.GONE);
                error_layout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onResponse(Object json) {
                loading_layout.setVisibility(View.GONE);
                String j = new Gson().toJson(json);
                TransResponse success = new Gson().fromJson(j,TransResponse.class);
                if (success.getUnits()!=null) {
                    if (success.getUnits().size() > 0) {
                        for (TransObject s : success.getUnits()) {
                            if (s.getUnit1_name().contains(getArguments().getString("text")) ||
                                    getArguments().getString("text").contains(s.getUnit1_name()) ||
                                    s.getUnit2_name().contains(getArguments().getString("text")) ||
                                    getArguments().getString("text").contains(s.getUnit2_name())) {
                                if (getArguments().getString("type").equals("after")) {
                                    if (s.getAmount_after_convert()!=null && !s.getAmount_after_convert().equals("")&&
                                    !s.getAmount_after_convert().equals("0")) {
                                        list.add(s);
                                        adapter.notifyDataSetChanged();
                                    }
                                } else {
                                    list.add(s);
                                    adapter.notifyDataSetChanged();
                                }
                            }
                        }
                        if (list.size() == 0) {
                            no_data_layout.setVisibility(View.VISIBLE);
                        }
                    } else {
                        no_data_layout.setVisibility(View.VISIBLE);
                    }
                }else {
                    no_data_layout.setVisibility(View.VISIBLE);
                }
            }
        }, new IFailure() {
            @Override
            public void onFailure() {
                loading_layout.setVisibility(View.GONE);
                no_internet_layout.setVisibility(View.VISIBLE);
            }
        });
    }
}
