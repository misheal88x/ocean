package com.smartstep.oceanapp.Fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.smartstep.oceanapp.Activities.MainActivity;
import com.smartstep.oceanapp.Bases.BaseFragment;
import com.smartstep.oceanapp.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class CalculatorFragment extends BaseFragment {

    private TextView from_currency,to_currency,to_value;
    private EditText from_value;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_calculator,container,false);
    }

    @Override
    public void init_views() {
        from_currency = base.findViewById(R.id.from_currency);
        to_currency = base.findViewById(R.id.to_currency);
        to_value = base.findViewById(R.id.to_value);
        from_value = base.findViewById(R.id.from_value);
    }

    @Override
    public void init_events() {
        from_value.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()>0){
                    Float value = Float.valueOf(getArguments().getString("value"));
                    Float new_value = Float.valueOf(from_value.getText().toString())*value;
                    to_value.setText(new_value+" "+getArguments().getString("to_unit_symbol"));
                }else {
                    to_value.setText(getArguments().getString("value")+" "+getArguments().getString("to_unit_symbol"));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        ((MainActivity)base).refresh_btn.setVisibility(View.GONE);
        ((MainActivity)base).search_layout.setVisibility(View.GONE);
        from_value.setText("1");
        from_currency.setText(getArguments().getString("from_unit"));
        to_currency.setText(getArguments().getString("to_unit"));
    }
}
