package com.smartstep.oceanapp.Fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.smartstep.oceanapp.APIsClasses.OffersAPIsClass;
import com.smartstep.oceanapp.Activities.MainActivity;
import com.smartstep.oceanapp.Adapter.OffersAdapter;
import com.smartstep.oceanapp.Adapter.TransmissionsAdapter;
import com.smartstep.oceanapp.Bases.App;
import com.smartstep.oceanapp.Bases.BaseFragment;
import com.smartstep.oceanapp.Bases.BaseFunctions;
import com.smartstep.oceanapp.Interfaces.IFailure;
import com.smartstep.oceanapp.Interfaces.IResponse;
import com.smartstep.oceanapp.Models.Offers_models.OfferObject;
import com.smartstep.oceanapp.Models.Offers_models.OffersResponse;
import com.smartstep.oceanapp.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class OffersFragment extends BaseFragment {

    private RecyclerView recyclerView;
    private List<OfferObject> list;
    private OffersAdapter adapter;
    private LinearLayoutManager layoutManager;

    private NestedScrollView scrollView;

    private LinearLayout loading_layout,no_data_layout,error_layout,no_internet_layout;

    private ProgressBar load_more;

    private int currentPage = 1;
    private boolean continue_paginate = true;
    private int per_page = 20;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_offers,container,false);
    }
    @Override
    public void init_views() {
        recyclerView = base.findViewById(R.id.recycler);
        loading_layout = base.findViewById(R.id.loading_layout);
        no_data_layout = base.findViewById(R.id.no_data_layout);
        error_layout = base.findViewById(R.id.error_layout);
        no_internet_layout = base.findViewById(R.id.no_internet_layout);
        scrollView = base.findViewById(R.id.scrollView);
        load_more = base.findViewById(R.id.load_more);
    }

    @Override
    public void init_events() {

        error_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callAPI(currentPage,0);
            }
        });
        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        if (list.size()>=per_page){
                            if (continue_paginate){
                                currentPage++;
                                callAPI(currentPage,1);
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        if (App.offers!=null && !App.offers.equals("")){
            ((TextView)base.findViewById(R.id.title)).setText(App.offers);
        }
        ((MainActivity)base).refresh_btn.setVisibility(View.GONE);
        ((MainActivity)base).search_layout.setVisibility(View.GONE);
        init_recycler();
        callAPI(currentPage,0);
    }
    private void init_recycler(){
        list = new ArrayList<>();
        adapter = new OffersAdapter(base,list);
        layoutManager = new LinearLayoutManager(base);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void callAPI(int page, final int type){
        if (type == 1){
            load_more.setVisibility(View.VISIBLE);
        }else {
            loading_layout.setVisibility(View.VISIBLE);
            no_data_layout.setVisibility(View.GONE);
            error_layout.setVisibility(View.GONE);
            no_internet_layout.setVisibility(View.GONE);
        }
        OffersAPIsClass.get_offers(
                base,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type);
                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        try {
                            OffersResponse success = new Gson().fromJson(j,OffersResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size()>0){
                                    process_data(type);
                                    per_page = success.getPer_page();
                                    for (OfferObject m : success.getData()){
                                        list.add(m);
                                        adapter.notifyDataSetChanged();
                                    }
                                }else {
                                    no_data(type);
                                }
                            }else {
                                error_happend(type);
                            }
                        }catch (Exception e){
                            if (type == 0){
                                error_happend(type);
                            }
                        }

                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        no_internet(type);
                    }
                });
    }

    private void error_happend(int type){
        if (type == 1){
            load_more.setVisibility(View.GONE);
        }else {
            loading_layout.setVisibility(View.GONE);
            error_layout.setVisibility(View.VISIBLE);
            no_data_layout.setVisibility(View.GONE);
            no_internet_layout.setVisibility(View.GONE);
        }
    }
    private void process_data(int type){
        if (type == 0){
            loading_layout.setVisibility(View.GONE);
            error_layout.setVisibility(View.GONE);
            no_data_layout.setVisibility(View.GONE);
            no_internet_layout.setVisibility(View.GONE);
        }else {
            load_more.setVisibility(View.GONE);
        }
    }
    private void no_data(int type){
        if (type == 0){
            loading_layout.setVisibility(View.GONE);
            no_data_layout.setVisibility(View.VISIBLE);
        }else {
            load_more.setVisibility(View.GONE);
            BaseFunctions.showWarningToast(base,"لا يوجد المزيد");
            continue_paginate = false;
        }
    }

    private void no_internet(int type){
        if (type == 0){
            loading_layout.setVisibility(View.GONE);
            no_internet_layout.setVisibility(View.VISIBLE);
        }else {
            load_more.setVisibility(View.GONE);
            BaseFunctions.showErrorToast(base,getResources().getString(R.string.no_internet));
        }
    }
}
