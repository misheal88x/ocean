package com.smartstep.oceanapp.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.smartstep.oceanapp.APIsClasses.ServicesAPIsClass;
import com.smartstep.oceanapp.Activities.MainActivity;
import com.smartstep.oceanapp.Adapter.OffersAdapter;
import com.smartstep.oceanapp.Adapter.ServicesAdapter;
import com.smartstep.oceanapp.Bases.App;
import com.smartstep.oceanapp.Bases.BaseFragment;
import com.smartstep.oceanapp.Interfaces.IFailure;
import com.smartstep.oceanapp.Interfaces.IResponse;
import com.smartstep.oceanapp.Models.Services_models.ServiceObject;
import com.smartstep.oceanapp.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ServicesFragment extends BaseFragment {

    private RecyclerView recyclerView;
    private ServicesAdapter adapter;
    private List<ServiceObject> list;
    private LinearLayoutManager layoutManager;

    private LinearLayout loading_layout,no_data_layout,error_layout,no_internet_layout;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_services,container,false);
    }
    @Override
    public void init_views() {
        recyclerView = base.findViewById(R.id.recycler);
        loading_layout = base.findViewById(R.id.loading_layout);
        no_data_layout = base.findViewById(R.id.no_data_layout);
        error_layout = base.findViewById(R.id.error_layout);
        no_internet_layout = base.findViewById(R.id.no_internet_layout);
    }

    @Override
    public void init_events() {
        error_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callAPI();
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        if (App.services!=null && !App.services.equals("")){
            ((TextView)base.findViewById(R.id.title)).setText(App.services);
        }
        ((MainActivity)base).refresh_btn.setVisibility(View.GONE);
        ((MainActivity)base).search_layout.setVisibility(View.GONE);
        init_recycler();
        callAPI();
    }

    private void init_recycler(){
        list = new ArrayList<>();
        adapter = new ServicesAdapter(base,list);
        layoutManager = new LinearLayoutManager(base);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void callAPI(){
        loading_layout.setVisibility(View.VISIBLE);
        no_data_layout.setVisibility(View.GONE);
        no_internet_layout.setVisibility(View.GONE);
        error_layout.setVisibility(View.GONE);
        ServicesAPIsClass.get_services(base, new IResponse() {
            @Override
            public void onResponse() {
                loading_layout.setVisibility(View.GONE);
                error_layout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onResponse(Object json) {
                loading_layout.setVisibility(View.GONE);
                String j = new Gson().toJson(json);
                try {
                    ServiceObject[] success = new Gson().fromJson(j, ServiceObject[].class);
                    if (success.length > 0) {
                        for (ServiceObject s : success) {
                            list.add(s);
                            adapter.notifyDataSetChanged();
                        }
                    } else {
                        no_data_layout.setVisibility(View.VISIBLE);
                    }
                }catch (Exception e){
                    callAPI();
                }
            }
        }, new IFailure() {
            @Override
            public void onFailure() {
                loading_layout.setVisibility(View.GONE);
                no_internet_layout.setVisibility(View.VISIBLE);
            }
        });
    }
}
