package com.smartstep.oceanapp.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.smartstep.oceanapp.APIsClasses.ServicesAPIsClass;
import com.smartstep.oceanapp.APIsClasses.TransmissionsAPIsClass;
import com.smartstep.oceanapp.Activities.MainActivity;
import com.smartstep.oceanapp.Adapter.TransmissionsAdapter;
import com.smartstep.oceanapp.Bases.App;
import com.smartstep.oceanapp.Bases.BaseFragment;
import com.smartstep.oceanapp.Bases.BaseFunctions;
import com.smartstep.oceanapp.Bases.SharedPrefManager;
import com.smartstep.oceanapp.Interfaces.IFailure;
import com.smartstep.oceanapp.Interfaces.IResponse;
import com.smartstep.oceanapp.Models.Services_models.ServiceObject;
import com.smartstep.oceanapp.Models.Start_models.StartObject;
import com.smartstep.oceanapp.Models.Transmissions_models.TransObject;
import com.smartstep.oceanapp.Models.Transmissions_models.TransResponse;
import com.smartstep.oceanapp.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class AfterTransmissionFragment extends BaseFragment {

    private RecyclerView recyclerView;
    private TransmissionsAdapter adapter;
    private LinearLayoutManager layoutManager;
    private RelativeLayout whatsapp;
    private List<TransObject> list;
    private TextView last_update;

    private LinearLayout loading_layout,no_data_layout,error_layout,no_internet_layout;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_after_transmission,container,false);
    }
    @Override
    public void init_views() {
        recyclerView = base.findViewById(R.id.recycler);
        whatsapp = base.findViewById(R.id.whatsapp);
        last_update = base.findViewById(R.id.last_update);
        loading_layout = base.findViewById(R.id.loading_layout);
        no_data_layout = base.findViewById(R.id.no_data_layout);
        error_layout = base.findViewById(R.id.error_layout);
        no_internet_layout = base.findViewById(R.id.no_internet_layout);
    }

    @Override
    public void init_events() {
        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number = "";
                for (StartObject s : SharedPrefManager.getInstance(base).getStartResponse()){
                    if (s.getSlug_name().equals("contact_us.whatsapp_phone")){
                        number = s.getValue().toString();
                        break;
                    }
                }
                if (number!= null &&!number.equals("")){
                    BaseFunctions.openWhatsapp(base,number.substring(1));
                }else {
                    BaseFunctions.showErrorToast(base,"لا يوجد رقم واتساب");
                }
            }
        });
        error_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callAPI();
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        if (App.after!=null && !App.after.equals("")){
            ((TextView)base.findViewById(R.id.title)).setText(App.after);
        }
        if (App.home!=null && !App.home.equals("")){
            ((TextView)base.findViewById(R.id.request)).setText(App.home);
        }
        ((MainActivity)base).refresh_btn.setVisibility(View.VISIBLE);
        ((MainActivity)base).search_layout.setVisibility(View.VISIBLE);
        init_recycler();
        callAPI();
    }

    private void init_recycler(){
        list = new ArrayList<>();
        adapter = new TransmissionsAdapter(base,list,"after");
        layoutManager = new LinearLayoutManager(base);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void callAPI(){
        loading_layout.setVisibility(View.VISIBLE);
        no_data_layout.setVisibility(View.GONE);
        no_internet_layout.setVisibility(View.GONE);
        error_layout.setVisibility(View.GONE);
        TransmissionsAPIsClass.get_units(base, new IResponse() {
            @Override
            public void onResponse() {
                loading_layout.setVisibility(View.GONE);
                error_layout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onResponse(Object json) {
                loading_layout.setVisibility(View.GONE);
                String j = new Gson().toJson(json);
                TransResponse success = new Gson().fromJson(j,TransResponse.class);
                if (success.getLast_update_date()!=null && !success.getLast_update_date().equals("")){
                    last_update.setText("اخر تحديث : "+BaseFunctions.dateExtractor(success.getLast_update_date()));
                }else {
                    last_update.setVisibility(View.GONE);
                }
                if (success.getUnits()!=null) {
                    if (success.getUnits().size() > 0) {
                        for (TransObject s : success.getUnits()) {
                            if (s.getAmount_after_convert()!=null && !s.getAmount_after_convert().equals("")&&
                                    !s.getAmount_after_convert().equals("0")) {
                                list.add(s);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    } else {
                        no_data_layout.setVisibility(View.VISIBLE);
                    }
                }else {
                    no_data_layout.setVisibility(View.VISIBLE);
                }
            }
        }, new IFailure() {
            @Override
            public void onFailure() {
                loading_layout.setVisibility(View.GONE);
                no_internet_layout.setVisibility(View.VISIBLE);
            }
        });
    }

    public void refresh(){
        list.clear();
        adapter = new TransmissionsAdapter(base,list,"after");
        recyclerView.setAdapter(adapter);
        callAPI();
    }
}
