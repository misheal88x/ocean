package com.smartstep.oceanapp.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.smartstep.oceanapp.APIsClasses.UserAPIsClass;
import com.smartstep.oceanapp.Bases.App;
import com.smartstep.oceanapp.Bases.BaseActivity;
import com.smartstep.oceanapp.Bases.BaseFunctions;
import com.smartstep.oceanapp.Bases.SharedPrefManager;
import com.smartstep.oceanapp.Fragments.AfterTransmissionFragment;
import com.smartstep.oceanapp.Fragments.BeforeTransmitionFragment;
import com.smartstep.oceanapp.Fragments.ContactFragment;
import com.smartstep.oceanapp.Fragments.OffersFragment;
import com.smartstep.oceanapp.Fragments.SearchResultFragment;
import com.smartstep.oceanapp.Fragments.ServicesFragment;
import com.smartstep.oceanapp.Interfaces.IFailure;
import com.smartstep.oceanapp.Interfaces.IResponse;
import com.smartstep.oceanapp.Models.Offers_models.OfferObject;
import com.smartstep.oceanapp.Models.Offers_models.OffersResponse;
import com.smartstep.oceanapp.Models.Start_models.StartObject;
import com.smartstep.oceanapp.R;

public class MainActivity extends BaseActivity {

    private DrawerLayout drawerLayout;
    private View drawer;
    private LinearLayout toolbar;
    private EditText search_edt;
    public RelativeLayout search_layout,search_btn;
    public ImageView refresh_btn,btn_drawer;
    private FrameLayout container;
    private FrameLayout ad_container;
    private AdView adView;
    private Fragment current_fragment;
    private boolean back_pressed = false;

    private TextView drawer_before,drawer_after,drawer_offers,drawer_services,drawer_contact;

    private LinearLayout bottom_back_layout,bottom_services_layout,bottom_contact_layout,bottom_offers_layout,bottom_before_layout,bottom_after_layout;
    private ImageView bottom_back_img,bottom_services_img,bottom_contact_img,bottom_offers_img,bottom_before_img,bottom_after_img;
    private TextView bottom_back_txt,bottom_services_txt,bottom_contact_txt,bottom_offers_txt,bottom_before_txt,bottom_after_txt;




    @Override
    public void set_layout() {
        setContentView(R.layout.activity_main);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {

        if (App.before!=null && !App.before.equals("")){
            drawer_before.setText(App.before);
            bottom_before_txt.setText(App.before);
        }
        if (App.after!=null && !App.after.equals("")){
            drawer_after.setText(App.after);
            bottom_after_txt.setText(App.after);
        }
        if (App.offers!=null && !App.offers.equals("")){
            drawer_offers.setText(App.offers);
            bottom_offers_txt.setText(App.offers);
        }
        if (App.services!=null && !App.services.equals("")){
            drawer_services.setText(App.services);
            bottom_services_txt.setText(App.services);
        }
        if (App.contact!=null && !App.contact.equals("")){
            drawer_contact.setText(App.contact);
            bottom_contact_txt.setText(App.contact);
        }
        click_before();
        adView = new AdView(MainActivity.this);
        for (StartObject s : SharedPrefManager.getInstance(MainActivity.this).getStartResponse()){
            if (s.getSlug_name().equals("app_config.allow_ads")){
                if (s.getValue().toString().equals("1")){
                    //adView.setAdUnitId("ca-app-pub-2993755530675918/1523517504");
                    //ad_container.addView(adView);
                    //loadBanner();
                }else {
                    ad_container.setVisibility(View.GONE);
                }
                break;
            }
        }

        //String token = FirebaseInstanceId.getInstance().getToken();
        //Log.i("newToken", "init_activity: "+token);
        //callUpdateTokenAPI(token);
    }

    @Override
    public void init_views() {
        //Toolbar
        toolbar = findViewById(R.id.toolbar);
        search_edt = toolbar.findViewById(R.id.search_edt);
        search_btn = toolbar.findViewById(R.id.search_btn);
        refresh_btn = toolbar.findViewById(R.id.refresh_btn);
        btn_drawer = toolbar.findViewById(R.id.btn_drawer);
        search_layout = toolbar.findViewById(R.id.search_layout);
        //Drawer
        drawerLayout = findViewById(R.id.drawer_layout);
        drawer = findViewById(R.id.rightDrawerMenu);
        drawer_before = findViewById(R.id.before_trans);
        drawer_after = findViewById(R.id.after_trans);
        drawer_offers = findViewById(R.id.offers);
        drawer_services = findViewById(R.id.services);
        drawer_contact = findViewById(R.id.contact);
        //Ads
        ad_container = findViewById(R.id.ad);
        //LinearLayout
        bottom_back_layout = findViewById(R.id.bottom_back_layout);
        bottom_services_layout = findViewById(R.id.bottom_services_layout);
        bottom_contact_layout = findViewById(R.id.bottom_contact_layout);
        bottom_offers_layout = findViewById(R.id.bottom_offers_layout);
        bottom_before_layout = findViewById(R.id.bottom_before_layout);
        bottom_after_layout = findViewById(R.id.bottom_after_layout);
        //ImageViews
        bottom_back_img = findViewById(R.id.bottom_back_img);
        bottom_services_img = findViewById(R.id.bottom_services_img);
        bottom_contact_img = findViewById(R.id.bottom_contact_img);
        bottom_offers_img = findViewById(R.id.bottom_offers_img);
        bottom_before_img = findViewById(R.id.bottom_before_img);
        bottom_after_img = findViewById(R.id.bottom_after_img);
        //TextViews
        bottom_back_txt = findViewById(R.id.bottom_back_txt);
        bottom_services_txt = findViewById(R.id.bottom_services_txt);
        bottom_contact_txt = findViewById(R.id.bottom_contact_txt);
        bottom_offers_txt = findViewById(R.id.bottom_offers_txt);
        bottom_before_txt = findViewById(R.id.bottom_before_txt);
        bottom_after_txt = findViewById(R.id.bottom_after_txt);
    }

    @Override
    public void init_events() {
        btn_drawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawerLayout.isDrawerOpen(drawer)){
                    drawerLayout.closeDrawer(drawer);
                }else {
                    drawerLayout.openDrawer(drawer);
                }
            }
        });

        refresh_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment f = fragmentManager.findFragmentById(R.id.container);
                if (f instanceof AfterTransmissionFragment){
                    AfterTransmissionFragment fragment = (AfterTransmissionFragment)f;
                    fragment.refresh();
                }else if (f instanceof BeforeTransmitionFragment){
                    BeforeTransmitionFragment fragment = (BeforeTransmitionFragment)f;
                    fragment.refresh();
                }
            }
        });

        search_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (search_edt.getText().toString().equals("")){
                    BaseFunctions.showWarningToast(MainActivity.this,getResources().getString(R.string.search_no_text));
                    return;
                }
                Fragment f = fragmentManager.findFragmentById(R.id.container);
                Bundle b = new Bundle();
                b.putString("text",search_edt.getText().toString());
                if (f instanceof AfterTransmissionFragment){
                    b.putString("type","after");
                }else if (f instanceof BeforeTransmitionFragment){
                    b.putString("type","before");
                }
                SearchResultFragment fragment = new SearchResultFragment();
                fragment.setArguments(b);
                open_fragment(fragment);
                search_edt.setText("");
            }
        });

        drawer_before.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click_before();
            }
        });
        drawer_after.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click_after();
            }
        });
        drawer_services.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click_services();
            }
        });
        drawer_offers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click_offers();
            }
        });
        drawer_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click_contact();
            }
        });
        bottom_back_layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                if (arg1.getAction()==MotionEvent.ACTION_DOWN) {
                    bottom_back_img.setImageResource(R.drawable.bottom_back_hover);
                    Fragment f = fragmentManager.findFragmentById(R.id.container);
                    if (f != null){
                        if (f instanceof BeforeTransmitionFragment){
                            if (!back_pressed){
                                back_pressed = true;
                                BaseFunctions.showInfoToast(MainActivity.this,getResources().getString(R.string.back_message));
                            }else {
                                Intent intent = new Intent(Intent.ACTION_MAIN);
                                intent.addCategory(Intent.CATEGORY_HOME);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            }
                        }else if (f instanceof AfterTransmissionFragment||
                                f instanceof OffersFragment ||
                                f instanceof ServicesFragment||
                                f instanceof ContactFragment){
                            click_before();
                            bottom_before_img.setImageResource(R.drawable.bottom_home_hover);
                            bottom_after_img.setImageResource(R.drawable.bottom_home);
                            bottom_offers_img.setImageResource(R.drawable.bottom_offer);
                            bottom_services_img.setImageResource(R.drawable.bottom_services);
                            bottom_contact_img.setImageResource(R.drawable.bottom_contact);
                        }else {
                            fragmentManager.popBackStack();
                        }
                    }
                }
                else if (arg1.getAction()==MotionEvent.ACTION_UP) {
                    bottom_back_img.setImageResource(R.drawable.bottom_back);
                }
                return true;
            }
        });
        bottom_before_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click_bottom_home();
            }
        });

        bottom_after_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click_after_home();
            }
        });

        bottom_offers_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click_bottom_offers();
            }
        });

        bottom_services_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click_bottom_services();
            }
        });

        bottom_contact_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click_bottom_contact();
            }
        });
    }

    @Override
    public void set_fragment_place() {
        fragment_place = findViewById(R.id.container);
    }

    private void loadBanner() {
        // Create an ad request. Check your logcat output for the hashed device ID
        // to get test ads on a physical device, e.g.,
        // "Use AdRequest.Builder.addTestDevice("ABCDE0123") to get test ads on this
        // device."
        AdRequest adRequest =
                new AdRequest.Builder()
                        //.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                        .build();

        AdSize adSize = getAdSize();
        // Step 4 - Set the adaptive ad size on the ad view.
        adView.setAdSize(adSize);

        // Step 5 - Start loading the ad in the background.
        adView.loadAd(adRequest);
    }

    private AdSize getAdSize() {
        // Step 2 - Determine the screen width (less decorations) to use for the ad width.
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        // Step 3 - Get adaptive ad size and return for setting on the ad view.
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    private void click_before(){
        if (current_fragment == null || !(current_fragment instanceof BeforeTransmitionFragment)){

            bottom_before_img.setImageResource(R.drawable.bottom_home_hover);
            bottom_after_img.setImageResource(R.drawable.bottom_home);
            bottom_offers_img.setImageResource(R.drawable.bottom_offer);
            bottom_services_img.setImageResource(R.drawable.bottom_services);
            bottom_contact_img.setImageResource(R.drawable.bottom_contact);

            drawer_before.setTextColor(getResources().getColor(R.color.colorPrimary));
            drawer_before.setBackgroundResource(R.drawable.rounded_white);

            drawer_after.setTextColor(getResources().getColor(R.color.white));
            drawer_after.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            drawer_offers.setTextColor(getResources().getColor(R.color.white));
            drawer_offers.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            drawer_services.setTextColor(getResources().getColor(R.color.white));
            drawer_services.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            drawer_contact.setTextColor(getResources().getColor(R.color.white));
            drawer_contact.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            BeforeTransmitionFragment fragment = new BeforeTransmitionFragment();
            current_fragment = fragment;
            open_fragment(fragment);
            drawerLayout.closeDrawer(drawer);
        }
    }

    private void click_after(){
        if (!(current_fragment instanceof AfterTransmissionFragment)){

            bottom_before_img.setImageResource(R.drawable.bottom_home);
            bottom_after_img.setImageResource(R.drawable.bottom_home_hover);
            bottom_offers_img.setImageResource(R.drawable.bottom_offer);
            bottom_services_img.setImageResource(R.drawable.bottom_services);
            bottom_contact_img.setImageResource(R.drawable.bottom_contact);

            drawer_before.setTextColor(getResources().getColor(R.color.white));
            drawer_before.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            drawer_after.setTextColor(getResources().getColor(R.color.colorPrimary));
            drawer_after.setBackgroundResource(R.drawable.rounded_white);

            drawer_offers.setTextColor(getResources().getColor(R.color.white));
            drawer_offers.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            drawer_services.setTextColor(getResources().getColor(R.color.white));
            drawer_services.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            drawer_contact.setTextColor(getResources().getColor(R.color.white));
            drawer_contact.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            AfterTransmissionFragment fragment = new AfterTransmissionFragment();
            current_fragment = fragment;
            open_fragment(fragment);
            drawerLayout.closeDrawer(drawer);
        }
    }

    private void click_offers(){
        if (!(current_fragment instanceof OffersFragment)){

            bottom_before_img.setImageResource(R.drawable.bottom_home);
            bottom_after_img.setImageResource(R.drawable.bottom_home);
            bottom_offers_img.setImageResource(R.drawable.bottom_offer_hover);
            bottom_services_img.setImageResource(R.drawable.bottom_services);
            bottom_contact_img.setImageResource(R.drawable.bottom_contact);

            drawer_before.setTextColor(getResources().getColor(R.color.white));
            drawer_before.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            drawer_after.setTextColor(getResources().getColor(R.color.white));
            drawer_after.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            drawer_offers.setTextColor(getResources().getColor(R.color.colorPrimary));
            drawer_offers.setBackgroundResource(R.drawable.rounded_white);

            drawer_services.setTextColor(getResources().getColor(R.color.white));
            drawer_services.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            drawer_contact.setTextColor(getResources().getColor(R.color.white));
            drawer_contact.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            OffersFragment fragment = new OffersFragment();
            current_fragment = fragment;
            open_fragment(fragment);
            drawerLayout.closeDrawer(drawer);
        }
    }

    private void click_services(){
        if (!(current_fragment instanceof ServicesFragment)){

            bottom_before_img.setImageResource(R.drawable.bottom_home);
            bottom_after_img.setImageResource(R.drawable.bottom_home);
            bottom_offers_img.setImageResource(R.drawable.bottom_offer);
            bottom_services_img.setImageResource(R.drawable.bottom_services_hover);
            bottom_contact_img.setImageResource(R.drawable.bottom_contact);

            drawer_before.setTextColor(getResources().getColor(R.color.white));
            drawer_before.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            drawer_after.setTextColor(getResources().getColor(R.color.white));
            drawer_after.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            drawer_offers.setTextColor(getResources().getColor(R.color.white));
            drawer_offers.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            drawer_services.setTextColor(getResources().getColor(R.color.colorPrimary));
            drawer_services.setBackgroundResource(R.drawable.rounded_white);

            drawer_contact.setTextColor(getResources().getColor(R.color.white));
            drawer_contact.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            ServicesFragment fragment = new ServicesFragment();
            current_fragment = fragment;
            open_fragment(fragment);
            drawerLayout.closeDrawer(drawer);
        }
    }

    private void click_contact(){
        if (!(current_fragment instanceof ContactFragment)){

            bottom_before_img.setImageResource(R.drawable.bottom_home);
            bottom_after_img.setImageResource(R.drawable.bottom_home);
            bottom_offers_img.setImageResource(R.drawable.bottom_offer);
            bottom_services_img.setImageResource(R.drawable.bottom_services);
            bottom_contact_img.setImageResource(R.drawable.bottom_contact_hover);

            drawer_before.setTextColor(getResources().getColor(R.color.white));
            drawer_before.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            drawer_after.setTextColor(getResources().getColor(R.color.white));
            drawer_after.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            drawer_offers.setTextColor(getResources().getColor(R.color.white));
            drawer_offers.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            drawer_services.setTextColor(getResources().getColor(R.color.white));
            drawer_services.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            drawer_contact.setTextColor(getResources().getColor(R.color.colorPrimary));
            drawer_contact.setBackgroundResource(R.drawable.rounded_white);

            ContactFragment fragment = new ContactFragment();
            current_fragment = fragment;
            open_fragment(fragment);
            drawerLayout.closeDrawer(drawer);
        }
    }

    @Override
    public void onBackPressed() {
        Fragment f = fragmentManager.findFragmentById(R.id.container);
        if (f != null){
            if (f instanceof BeforeTransmitionFragment){
                if (!back_pressed){
                    back_pressed = true;
                    BaseFunctions.showInfoToast(MainActivity.this,getResources().getString(R.string.back_message));
                }else {
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }
            }else if (f instanceof AfterTransmissionFragment||
                    f instanceof OffersFragment ||
                    f instanceof ServicesFragment||
                    f instanceof ContactFragment){
                click_before();
            }else {
                fragmentManager.popBackStack();
            }
        }
    }

    private void click_bottom_home(){
        bottom_before_img.setImageResource(R.drawable.bottom_home_hover);
        bottom_after_img.setImageResource(R.drawable.bottom_home);
        bottom_offers_img.setImageResource(R.drawable.bottom_offer);
        bottom_services_img.setImageResource(R.drawable.bottom_services);
        bottom_contact_img.setImageResource(R.drawable.bottom_contact);

        Fragment f = fragmentManager.findFragmentById(R.id.container);
        if (f instanceof BeforeTransmitionFragment){

        }else {
            click_before();
        }
    }
    private void click_after_home(){
        bottom_before_img.setImageResource(R.drawable.bottom_home);
        bottom_after_img.setImageResource(R.drawable.bottom_home_hover);
        bottom_offers_img.setImageResource(R.drawable.bottom_offer);
        bottom_services_img.setImageResource(R.drawable.bottom_services);
        bottom_contact_img.setImageResource(R.drawable.bottom_contact);

        Fragment f = fragmentManager.findFragmentById(R.id.container);
        if (f instanceof AfterTransmissionFragment){

        }else {
            click_after();
        }
    }
    private void click_bottom_offers(){
        bottom_before_img.setImageResource(R.drawable.bottom_home);
        bottom_after_img.setImageResource(R.drawable.bottom_home);
        bottom_offers_img.setImageResource(R.drawable.bottom_offer_hover);
        bottom_services_img.setImageResource(R.drawable.bottom_services);
        bottom_contact_img.setImageResource(R.drawable.bottom_contact);

        Fragment f = fragmentManager.findFragmentById(R.id.container);
        if (f instanceof OffersFragment){

        }else {
            click_offers();
        }
    }
    private void click_bottom_services(){
        bottom_before_img.setImageResource(R.drawable.bottom_home);
        bottom_after_img.setImageResource(R.drawable.bottom_home);
        bottom_offers_img.setImageResource(R.drawable.bottom_offer);
        bottom_services_img.setImageResource(R.drawable.bottom_services_hover);
        bottom_contact_img.setImageResource(R.drawable.bottom_contact);

        Fragment f = fragmentManager.findFragmentById(R.id.container);
        if (f instanceof ServicesFragment){

        }else {
            click_services();
        }
    }
    private void click_bottom_contact(){
        bottom_before_img.setImageResource(R.drawable.bottom_home);
        bottom_after_img.setImageResource(R.drawable.bottom_home);
        bottom_offers_img.setImageResource(R.drawable.bottom_offer);
        bottom_services_img.setImageResource(R.drawable.bottom_services);
        bottom_contact_img.setImageResource(R.drawable.bottom_contact_hover);

        Fragment f = fragmentManager.findFragmentById(R.id.container);
        if (f instanceof ContactFragment){

        }else {
            click_contact();
        }
    }

    private void callUpdateTokenAPI(String token){
        UserAPIsClass.update_token(MainActivity.this,
                token,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        Log.i("firebase_token_update", "onResponse: "+"Failed");
                    }

                    @Override
                    public void onResponse(Object json) {
                        Log.i("firebase_token_update", "onResponse: "+"Success");
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Log.i("firebase_token_update", "onResponse: "+"No Internet");
                    }
                });
    }
}
