package com.smartstep.oceanapp.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.smartstep.oceanapp.APIsClasses.StartAPIsClass;
import com.smartstep.oceanapp.Bases.App;
import com.smartstep.oceanapp.Bases.BaseActivity;
import com.smartstep.oceanapp.Bases.BaseFunctions;
import com.smartstep.oceanapp.Bases.SharedPrefManager;
import com.smartstep.oceanapp.Interfaces.IFailure;
import com.smartstep.oceanapp.Interfaces.IResponse;
import com.smartstep.oceanapp.Models.Start_models.StartObject;
import com.smartstep.oceanapp.Models.Start_models.StartResponse;
import com.smartstep.oceanapp.R;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AlertDialog;

public class SplashActivity extends BaseActivity {

    private RelativeLayout layout;
    //private InterstitialAd mInterstitialAd;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_splash);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        if (BaseFunctions.isOnline(SplashActivity.this)){
            callAPI();
        }else {
            startActivity(new Intent(SplashActivity.this,NoInternetActivity.class));
            finish();
        }
        /*
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-2993755530675918/1991017839");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.show();
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                mInterstitialAd.show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
                if (BaseFunctions.isOnline(SplashActivity.this)){
                    callAPI();
                }else {
                    startActivity(new Intent(SplashActivity.this,NoInternetActivity.class));
                    finish();
                }
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the interstitial ad is closed.
                if (BaseFunctions.isOnline(SplashActivity.this)){
                    callAPI();
                }else {
                    startActivity(new Intent(SplashActivity.this,NoInternetActivity.class));
                    finish();
                }
            }
        });


         */

    }

    @Override
    public void init_views() {
        layout = findViewById(R.id.layout);
    }

    @Override
    public void init_events() {

    }

    @Override
    public void set_fragment_place() {

    }

    private void callAPI(){
        StartAPIsClass.get_init(SplashActivity.this,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        Snackbar.make(layout, getResources().getString(R.string.error_occured_2), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        StartResponse success = new Gson().fromJson(j,StartResponse.class);
                        List<StartObject> list = new ArrayList<>();
                        if (success.getConfig()!=null){
                            if (success.getConfig().size() > 0){
                                for (StartObject s : success.getConfig()){
                                    switch (s.getSlug_name()){
                                        case "app_config.screen_title.price_before":{
                                            App.before = s.getValue().toString();
                                        }break;
                                        case "app_config.screen_title.price_after":{
                                            App.after = s.getValue().toString();
                                        }break;
                                        case "app_config.screen_title.offers":{
                                            App.offers = s.getValue().toString();
                                        }break;
                                        case "app_config.screen_title.services":{
                                            App.services = s.getValue().toString();
                                        }break;
                                        case "app_config.screen_title.contact_us":{
                                            App.contact = s.getValue().toString();
                                        }break;
                                        case "app_config.content.about_us":{
                                            App.about = s.getValue().toString();
                                        }break;
                                        case "app_config.content.request_service":{
                                            App.home = s.getValue().toString();
                                        }break;
                                    }
                                    list.add(s);
                                }
                            }
                        }
                        SharedPrefManager.getInstance(SplashActivity.this).setStartResponse(list);
                        processData();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(layout, getResources().getString(R.string.error_occured_2), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    private void processData(){
        String online_version = "";
        String force_update = "";
        String google_play_link = "";
        for (StartObject s : SharedPrefManager.getInstance(SplashActivity.this).getStartResponse()){
            if (s.getSlug_name().equals("app_config.version")){
                online_version = s.getValue().toString();
            }else if (s.getSlug_name().equals("app_config.force_update")){
                force_update = s.getValue().toString();
            }else if (s.getSlug_name().equals("app_config.google_play_app_link")){
                google_play_link = s.getValue().toString();
            }
        }
        if (Float.valueOf(String.valueOf(BaseFunctions.getVersionCode(SplashActivity.this)))<Float.valueOf(online_version)){
            if (force_update.equals("0")){
                AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
                builder.setIcon(R.drawable.ocean_logo_launcher);
                builder.setCancelable(false);
                builder.setTitle("تحديث");
                builder.setMessage("نسختك من تطبيق أوشن أصبحت قديمة, اضغط على تحديث لتحميل النسخة الجديدة من متجر جوجل بلاي");
                builder.setPositiveButton("تحديث", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        BaseFunctions.lunchPlayStore(SplashActivity.this,SplashActivity.this.getPackageName());
                        finish();
                    }
                });
                builder.setNegativeButton("متابعة", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(SplashActivity.this,MainActivity.class));
                        finish();
                    }
                });
                builder.show();
            }else {
                AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
                builder.setIcon(R.drawable.ocean_logo_launcher);
                builder.setCancelable(false);
                builder.setTitle("تحديث");
                builder.setMessage("نسختك من تطبيق أوشن أصبحت قديمة, اضغط على تحديث لتحميل النسخة الجديدة من متجر جوجل بلاي");
                builder.setPositiveButton("تحديث", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        BaseFunctions.lunchPlayStore(SplashActivity.this,SplashActivity.this.getPackageName());
                        finish();
                    }
                });
                builder.show();
            }
        }else {
            startActivity(new Intent(SplashActivity.this,MainActivity.class));
            finish();
        }
    }
}
