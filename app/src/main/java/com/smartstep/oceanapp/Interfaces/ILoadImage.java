package com.smartstep.oceanapp.Interfaces;

public interface ILoadImage {
    void onLoaded();
    void onFailed();
}
