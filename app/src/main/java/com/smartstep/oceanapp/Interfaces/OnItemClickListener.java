package com.smartstep.oceanapp.Interfaces;

public interface OnItemClickListener {
    void onItemClick(int position);
}
