package com.smartstep.oceanapp.Interfaces;

public interface IResponse {
    void onResponse();
    void onResponse(Object json);
}
