package com.smartstep.oceanapp.Bases;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.smartstep.oceanapp.Interfaces.IFailure;
import com.smartstep.oceanapp.Interfaces.IResponse;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseRetrofit {
    public static final String BASE_URL = "http://ocean-ar.net/public/api/";
    public static Retrofit retrofit;
    public static IResponse onResponse;
    public static IFailure onFailure;
    public static Retrofit configureRetrofitWithoutBearer(){
        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(10, TimeUnit.MINUTES).build();
        retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        return retrofit;
    }

    //public static Retrofit configureRetrofitWithBearer(Context context){
        /*
        final String access_token = SharedPrefManager.getInstance(context).getUser().getApi_token();
        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.MINUTES)
                .readTimeout(10, TimeUnit.MINUTES)
                .writeTimeout(10, TimeUnit.MINUTES).addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request newRequest  = chain.request().newBuilder()
                                .addHeader("Authorization", "Bearer " + access_token)
                                .build();
                        return chain.proceed(newRequest);
                    }
                }).build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create(gson)).build();
        return retrofit;

         */
    //}
}
