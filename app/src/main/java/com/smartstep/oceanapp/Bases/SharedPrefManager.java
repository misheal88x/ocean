package com.smartstep.oceanapp.Bases;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.smartstep.oceanapp.Models.Start_models.StartObject;

import java.util.ArrayList;
import java.util.List;

public class SharedPrefManager {
    private static SharedPrefManager mInstance;
    private static Context mCtx;

    private static final String SHARED_PREF_NAME = "Main";
    private static final String VIEW_LANGUAGE = "view_language";
    private static final String START_RESPONSE = "start_response";

    private SharedPrefManager(Context context) {
        mCtx = context;
    }
    public static synchronized SharedPrefManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(context);
        }
        return mInstance;
    }

    public void setViewLanguage(String lan){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(VIEW_LANGUAGE,lan);
        editor.commit();
    }

    public String getViewLanguage() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String value = sharedPreferences.getString(VIEW_LANGUAGE, "en");
        return value;
    }

    public void setStartResponse(List<StartObject> list){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(START_RESPONSE,new Gson().toJson(list));
        editor.commit();
    }

    public List<StartObject> getStartResponse() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String json = sharedPreferences.getString(START_RESPONSE, "[]");
        StartObject[] success = new Gson().fromJson(json,StartObject[].class);
        List<StartObject> list = new ArrayList<>();
        if (success.length > 0){
            for (StartObject s : success){
                list.add(s);
            }
        }
        return list;
    }
}
