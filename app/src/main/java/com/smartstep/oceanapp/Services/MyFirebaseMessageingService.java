package com.smartstep.oceanapp.Services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.smartstep.oceanapp.APIsClasses.UserAPIsClass;
import com.smartstep.oceanapp.Activities.MainActivity;
import com.smartstep.oceanapp.Activities.SplashActivity;
import com.smartstep.oceanapp.Bases.BaseFunctions;
import com.smartstep.oceanapp.Interfaces.IFailure;
import com.smartstep.oceanapp.Interfaces.IResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

public class MyFirebaseMessageingService extends FirebaseMessagingService {
    final int MY_NOTIFICATION_ID = 1;
    final String NOTIFICATION_CHANNEL_ID = "10001";
    NotificationManager notificationManager;
    NotificationCompat.Builder builder;
    //final Context context = this;

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        String recent_token = FirebaseInstanceId.getInstance().getToken();
        Log.e("newToken1",recent_token);
        callUpdateTokenAPI(recent_token);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            Map<String, String> params = remoteMessage.getData();
            JSONObject jsonObject = new JSONObject(params);
            Log.i("JSON_OBJECTT", "the : " + jsonObject.toString());
            sendNotification(jsonObject);
        }catch (Exception e){
            Log.i("JSON_OBJECTT", "onMessageReceived: "+e.getMessage());
        }
    }
    private void sendNotification(JSONObject jsonObject) {

        String content = "";

        try {
            if (jsonObject.has("content"))
                content = jsonObject.getString("content");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                getApplicationContext(),
                1,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT
                        | PendingIntent.FLAG_ONE_SHOT);
        BaseFunctions.showNotification(getApplicationContext(), pendingIntent, content, builder, notificationManager, NOTIFICATION_CHANNEL_ID, MY_NOTIFICATION_ID);
    }

    private void callUpdateTokenAPI(String token){
        UserAPIsClass.update_token(MyFirebaseMessageingService.this,
                token,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        Log.i("firebase_token_update", "onResponse: "+"Failed");
                    }

                    @Override
                    public void onResponse(Object json) {
                        Log.i("firebase_token_update", "onResponse: "+"Success");
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Log.i("firebase_token_update", "onResponse: "+"No Internet");
                    }
                });
    }
}
