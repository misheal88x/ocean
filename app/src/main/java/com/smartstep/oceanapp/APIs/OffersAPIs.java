package com.smartstep.oceanapp.APIs;

import com.smartstep.oceanapp.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface OffersAPIs {

    @GET("offers")
    Call<BaseResponse> get_offers(
            @Header("DEVICEID") String DEVICEID,
            @Query("page") int page);
}
