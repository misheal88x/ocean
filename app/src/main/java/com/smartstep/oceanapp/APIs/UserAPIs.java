package com.smartstep.oceanapp.APIs;

import com.smartstep.oceanapp.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface UserAPIs {
    @FormUrlEncoded
    @POST("device/fcm/update")
    Call<Object> update_token(
            @Header("DEVICEID") String DEVICEID,
            @Field("fcm_token") String fcm_token
    );
}
