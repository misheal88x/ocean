package com.smartstep.oceanapp.APIs;

import com.smartstep.oceanapp.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface StartAPIs {
    @GET("init-app")
    Call<BaseResponse> get_init(@Header("DEVICEID")String DEVICEID);
}
