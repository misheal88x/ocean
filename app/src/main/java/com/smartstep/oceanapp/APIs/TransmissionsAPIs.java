package com.smartstep.oceanapp.APIs;

import com.smartstep.oceanapp.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public interface TransmissionsAPIs {
    @GET("units")
    Call<BaseResponse> get_units(@Header("DEVICEID") String DEVICEID);

    @GET("units/{id}")
    Call<BaseResponse> getUnit(@Header("DEVICEID") String DEVICEID, @Path("id") String id);
}
