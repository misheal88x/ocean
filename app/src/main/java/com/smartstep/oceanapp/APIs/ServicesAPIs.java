package com.smartstep.oceanapp.APIs;

import com.smartstep.oceanapp.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface ServicesAPIs {
    @GET("services")
    Call<BaseResponse> get_services(@Header("DEVICEID") String DEVICEID);
}
